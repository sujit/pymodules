import fnmatch, itertools, os, re
import OleFileIO_PL as olemodule

files = []
for i in os.listdir('.'):
	if fnmatch.fnmatch(i, '*.bin') is True:
		files.insert(0, i)

allStreams = []
for eachFile in files:
	flag = olemodule.isOleFile(eachFile)
	if flag is True:
		print '\n[+] File:', eachFile
		readOLE = olemodule.OleFileIO(eachFile)
		allStreams = list(itertools.chain.from_iterable(readOLE.listdir()))
		for i in allStreams:
			if re.findall('(\x5c[0-9a-zA-Z]){5,}', i):
				pass
			else:
				allStreams.insert(0, allStreams)

		print allStreams