Python Modules:
================
Install bpython
Install python-dev since it contains very useful headers and libraries.

pcap-reassembler [Reassembles UDP/TCP packets into application layer messages]
pydermonkey [Install it through easy_install command line] Low-level Javascript Emulator Engine.
python-spidermonkey [Mozilla Firefox JavaScript Engine]
python-fabric [Library and command-line tool for streamlining the use of SSH for application deployment or systems administration tasks]
bcrypt [Strong Password hashing algorithm (blowfish hashing): http://www.mindrot.org/projects/py-bcrypt/]
bottle [Python Web Framework: http://bottlepy.org/docs/dev/]
python-bleach [HTML Sanitization tool]
python-xmltodict
python-argcomplete
python-pyip [Raw IP Packet Assembling/Disassembling]
HTTPie [A Curl like HTTP tool based on Python]
setuptools
iniparse [Parse ini files]
poster [Upload files through POST request using multipart/formdata encoding]
pycurl [Upload files through POST http://goo.gl/QGKMP and http://goo.gl/gR4pb]
loxun
python-scrapy
pyzilla
bztools [http://christian.legnitto.com/blog/2010/12/27/python-library-for-bugzillas-rest-api/]
python-scrapy-doc
python-bzutils [Module to interact with Bugzilla database]
pip
python-subversion
python-svn
pylibpcap
pysmb
pysmbc
python-bugzilla
web2py
requests
urllib3
httplib2
pcapy
dpkt
pycrypto
BeautifulSoup
pcapy [For Python v2.5, v2.6 and v2.7: http://breakingcode.wordpress.com/2012/07/16/quickpost-updated-impacketpcapy-installers-for-python-2-5-2-6-2-7/]
paramiko [winscp client] https://pypi.python.org/pypi/scpclient
scapy
xgoogle
python-configparser
python-iniparse
impacket
pydbg
ctypes
termcolor
pip
mechanize
python-optcomplete [bash completion for Python programs]
selenium
pydbgeng [Windows]
pycap
pamie [IE Automation Module]
libxml2
flask
modulefinder
optparse
xlrd
xlwt
pyExcelerator
xlutils
lxml
virtualenv
pywin32
pexpect
cmd
pymssql
simplejson
json
mimetools
pcapy
netaddr
pyssh
hachoir
smbc
urlgrabber [Simpifies fetching of files for several cross-protocols]
urlparse


Selenium Alternatives:
* mechanize
* spynner       [https://code.google.com/p/spynner/  OR  https://github.com/makinacorpus/spynner]
* twill         [http://twill.idyll.org/python-api.html]
* Windmill
* Pywebkitgtk   [http://blog.motane.lu/2009/06/18/pywebkitgtk-execute-javascript-from-python/]
* QtWebKit      [http://blog.motane.lu/2009/07/07/downloading-a-pages-content-with-python-and-webkit/]


peframe         [Static PE File Analysis Module: https://code.google.com/p/peframe/]
peslice         [Win32 Binary Parser: https://code.google.com/p/peslice]
defuzz          [Generates files byte by byte between a crash and an original file: https://code.google.com/p/defuzz/]
httpthief       [HTTP daemon for automated cookie & http basic auth credentials harvestering: https://code.google.com/p/httpthief/]
mir             [Automated malware analysis framework: https://code.google.com/p/mir/]
malfind2        [Find malicious Javascript and HTML: https://code.google.com/p/malfind2/]
usbdump2        [Silently copies contents from an inserted USB Disk: https://code.google.com/p/usbdump/]
hackpack2013    [Collection of several utilities: https://code.google.com/p/hackpak2013/]
immunity        [Execute your browser in a securely manner: https://code.google.com/p/immunity/]
pyfiscan        [Web-application vulnerability version scanner: https://code.google.com/p/pyfiscan/]
xsscan          [XSS Scanner: https://code.google.com/p/xss-scanner/]
cuddle-fuzz     [Collection of several utilities: https://code.google.com/p/cuddle-fuzz/]
webenum         [Enumerate HTTP Responses using dynamically generated queries: https://code.google.com/p/webenum/]
pyew            [Tool for Static malware analysis: https://code.google.com/p/pyew/]
                [http://sysexit.wordpress.com/2011/12/17/installing-inguma-bokken-pyew-and-radare2-in-ubuntu/]


3rd party useful utilities:
===========================
Online HTTP Request Generator Tools:
  * http://apikitchen.com/
  * http://www.hurl.it/

bugz [Command line interface to Bugzilla]
Documentation: http://www.liquidx.net/pybugz/

BugZilla Tools
http://pypi.python.org/pypi/bugzillatools/

Command: bugzilla
Documentation: https://fedorahosted.org/python-bugzilla/wiki/BugzillaManPage
