#! /usr/bin/python

""" 
    Name:   PEslice
    Author: Ramece Cave
    Email:  devmece@gmail.com
    
    License: BSD
    
    Copyright (c) 2012, Ramece Cave
    All rights reserved.

    Redistribution and use in source and binary forms, with or without modification, are permitted      
    provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions 
    and the following disclaimer. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES,INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
    DAMAGE.
"""

__version__ = "1.0.1"
import sys,binascii,pefile,re

class peslice():
    def __init__(self,win32Binary):
        self.win32Binary = win32Binary
        self.pe = pefile.PE(win32Binary)
    
    @staticmethod
    def CheckType(pe):
        """
        Checks if the Image Characteristics of the specified binary is between 0x2000 and 0x3000.
        """
        
        fileChar = peslice.GetField("file","characteristics",pe).split("=")[-1]

        if int(fileChar,16) >= int(0x2000) and int(fileChar,16) < int(0x3000):
            return "DLL"
        else:
            return "EXE"
        
    @staticmethod
    def GetField(header,field,pe):
        """
        Retrieves the value for the specified field.
        """
        
        header = peslice.GetImageHeader(pe,header)
        fieldList = peslice.GetHeaderFields(pe,header)
        
        for fieldName in fieldList:
            if re.match(field, fieldName, re.IGNORECASE):
                field = fieldName
                fieldValue = field+"="+str(hex(getattr(header,field)))
                return fieldValue
        
        return None
    
    @staticmethod
    def GetDataDir(pe):
        """
        Returns the used and unused data directories.
        """
        
        numOfDirs = pe.OPTIONAL_HEADER.NumberOfRvaAndSizes
        dataDirs = pe.OPTIONAL_HEADER.DATA_DIRECTORY
        usedDirs = []
        unusedDirs = []
        
        for dir in dataDirs:
            dirName = str(dir).split()[0]
            dirVa = str(dir).split()[2]
            dirSize = int(str(dir).split()[-1], 16)
            
            if dirSize != 0:
                usedDir = dirName,"RVA:",dirVa,"SIZE:",str(dirSize)
                usedDir = ' '.join(usedDir)
                usedDirs.append(usedDir)
            elif dirSize > 0:
                unusedDirs.append(dirName)
        
        return usedDirs,unusedDirs,numOfDirs
    
    @staticmethod
    def GetExportTable(pe):
        """
        Returns the Export Address Table (EAT).
        """
        
        eat = []

        try:
            for exportEntry in pe.DIRECTORY_ENTRY_EXPORT.symbols:
                eat.append(exportEntry.name)
        except:
            pass
            
            return 0
            
        return eat
    
    @staticmethod
    def GetHeaderFields(pe,header):
        """
        Lists all fields in the specified header.
        """
        if header.name == "IMAGE_DOS_HEADER":
            fieldFindRegex = '^e+_.*'
        else:
            fieldFindRegex = '^[A-Z][a-z].*[a-z]$'
        
        header = peslice.GetImageHeader(pe,header)
        fieldList = []
        
        for field in header.__dict__:
            if re.match(fieldFindRegex,field):
                fieldList.append(field)
        
        return fieldList
    
    @staticmethod
    def GetImportTable(pe):
        """
        Returns the Import Address Table (IAT).
        """
        
        iat = []
        
        for importEntry in pe.DIRECTORY_ENTRY_IMPORT:
            for importFunc in importEntry.imports:
                dllAndFunction = importEntry.dll,importFunc.name
                iat.append(dllAndFunction)
                
        return iat
    
    @staticmethod
    def GetImageHeader(pe,header):
        """
        Returns the correct header based on the provided option.
        """
        
        if "dos" == header:
            header = pe.DOS_HEADER
        if "file" == header:
            header = pe.FILE_HEADER
        if "nt" == header:
            header = pe.NT_HEADERS
        if "optional" == header:
            header = pe.OPTIONAL_HEADER
        
        return header
    
    @staticmethod
    def GetSections(pe):
        """
        Returns the following values:
        Section Name, VirtualSize (hex), VirtualAddress (hex), SizeOfRawData (hex)
        PointerToRawData (dec)
        """
        
        sectionNames = []
        for section in pe.sections:
            sectionInfo = section.Name,str(hex(section.Misc_VirtualSize)),str(hex(\
            section.VirtualAddress)),str(hex(section.SizeOfRawData)),str(hex(\
            section.PointerToRawData)),str(section.PointerToRawData)
            sectionInfo = ' '.join(sectionInfo)
            sectionNames.append(sectionInfo)
            
        return sectionNames
    
    @staticmethod
    def PrintArgHandler(cli,option,pe):
        """
        Handles the processing and printing for the selected options,
        using corresponding classes. <CLI>
        """
        
        params = {}
        params["exe"] = cli[1]
        params["length"] = len(cli) 
        
        if "=" in option:
            option,header = option.replace('--','').split("=")
            params["option"] = option
            params["header"] = header
            if len(cli) == 4:
                field = cli[-1]
                params["field"] = field
            elif len(cli) > 4:
                print "Error: Incorrect syntax or options."
                sys.exit()
        else:
            option = option.replace('--','')
            params["option"] = option
            
        arguments = {
                        "check" : "CheckOptionHandler",
                        "data" : "DataOptionHandler",
                        "export" : "ExportOptionHandler",
                        "field" :  "FieldOptionHandler",
                        "get" : "GetOptionHandler",
                        "import" : "ImportOptionHandler",
                        "sections" : "SectOptionHandler"
                    }
                    
        class CheckOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "check" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    exe = cli[1]
                    fileType = peslice.CheckType(pe)
                    print exe,"=",fileType
                
                return 0
        class DataOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "data" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    results = peslice.GetDataDir(pe)
                    usedDirs = results[0]
                    unusedDirs = results[1]
                    totalDirs = results[2]
            
                    print "Total number of data directories:",totalDirs,"\n"
                    for dir in usedDirs:
                        print dir.replace("[","").replace("]","")
                    print "\r"
                    for dir in unusedDirs:
                        print dir.replace("[","").replace("]","")
                    
                    return 0
        class ExportOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "export" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    results = peslice.GetExportTable(pe)
                    
                    if results:
                        for result in results:
                            print result
                    elif not results:
                        print "No exports found."
                    
                    return 0
        class FieldOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "field" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    fieldGet = peslice.GetHeaderFields(pe,args.get("header"))
                
                for entry in fieldGet:
                    print entry
                
                return 0
        
        class GetOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "get" or args.get("length") > 4:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    header = args.get("header")
                    lookupField = args.get("field")
                    try:
                        GetFieldResults = peslice.GetField(header,lookupField,pe)
                    except:
                        print "Error: incorrect syntax or optoins"
                        sys.exit()
                        
                    if not GetFieldResults:
                        print lookupField,"-","is not a valid field","\n"
                        print "Run peslice.py",args.get("exe"),"--field="+header+" for a list of valid fields."
                        sys.exit()
                    else:
                        print GetFieldResults
                    
                    return 0
        
        class ImportOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "import" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    importTable = peslice.GetImportTable(pe)
                    
                    for (dllName,function) in importTable:
                        print dllName,function
                
                return 0

        class SectOptionHandler():
            @staticmethod
            def Process(args):
                if args.get("option") != "sections" or args.get("length") > 3:
                    print "Error: incorrect syntax or options"
                    sys.exit()
                else:
                    sectionNames = peslice.GetSections(pe)
                    print "Name","|","Virtual Size","|","Virtual Offset","|","Raw Size","|","Raw Offset","\n"
                    for nameInfo in sectionNames:
                        print nameInfo
                
                return 0

        if option in arguments.keys():
            selectedOption = vars()[arguments.get(option)]
            selectedOption.Process(params)
        else:
            print "Error: Incorrect syntax or options."
            sys.exit()
        
    @staticmethod
    def main():
        try:
            cli = sys.argv
            exe = cli[1]
        except:
            print "Usage: peslice.py <exe/dll> <option>"
            print "\r"
            print "PE32 Headers:"
            print "\t","dos = DOS HEADER"
            print "\t","file = FILE HEADER"
            print "\t","nt = NT HEADERS"
            print "\t","optional = OPTIONAL HEADER"
            print "\r"
            print "Options:"
            print "\t","--check = Check if binary is a DLL or EXE"
            print "\t","--data = List allocated and unallocated data directories" 
            print "\t","--export = List Export Address Table (EAT)"
            print "\t","--field=HEADER = List all fields in header"
            print "\t","--get=HEADER <field> = Retreive field value from header"
            print "\t","--import = List Import Address Table (IAT)"
            print "\t","--sections = List all sections"
            print ""
            
            sys.exit()
            
        try:
            option = cli[2]
            pe = pefile.PE(exe)
        except:
            print "Error: Syntax or wrong file type"
            sys.exit()
            
        peslice.PrintArgHandler(cli,option,pe)

if __name__=='__main__':
    peslice.main() 
